import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class EmployeeGreaterAgeSpecs {
    @Test
    public void itSatisfiesEmployeeAge() {
        assertThat(EmployeeAge.is(Greater.than(8)).satisfy(new Employee(null, 5))).isFalse();
        assertThat(EmployeeAge.is(Greater.than(6)).satisfy(new Employee(null, 7))).isTrue();
    }
}
