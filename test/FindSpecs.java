import org.junit.Test;

import java.util.Arrays;

import static org.fest.assertions.Assertions.assertThat;

public class FindSpecs {

    @Test
    public void itAddsAWhereClause() {
        Employee expectedEmployee = new Employee("foo", 1);
        Find find = new Find(Arrays.asList(expectedEmployee, new Employee("foo", 2)));
        assertThat(find.where(EmployeeAge.is(Equal.to(1))).execute()).isEqualTo(Arrays.asList(expectedEmployee));
    }

    @Test
    public void itAddsAnAndClause() {
        Employee employee1 = new Employee("foo", 1);
        Employee employee2 = new Employee("bar", 2);
        Employee employee3 = new Employee("foobar", 2);
        Find find = new Find(Arrays.asList(employee1, employee2, employee3));
        FindWhere clause = find.where(EmployeeAge.is(Greater.than(1))).and(EmployeeName.is(Equal.to("foobar")));
        assertThat(clause.execute()).isEqualTo(Arrays.asList(employee3));
    }
    
    @Test
    public void itAddsAnAndClauseToAnAndClause() {
        Employee employee1 = new Employee("foo", 1);
        Employee employee2 = new Employee("bar", 2);
        Employee employee3 = new Employee("foobar", 2);
        Employee employee4 = new Employee("foobar", 6);
        Find find = new Find(Arrays.asList(employee1, employee2, employee3, employee4));
        FindWhere clause = find.where(EmployeeAge.is(Greater.than(1))).and(EmployeeName.is(Equal.to("foobar"))).and(EmployeeAge.is(Lesser.than(4)));
        assertThat(clause.execute()).isEqualTo(Arrays.asList(employee3));
    }
}
