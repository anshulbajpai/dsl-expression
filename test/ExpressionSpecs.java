import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class ExpressionSpecs {
    @Test
    public void itCreatesAFindExpression() {

        List<Employee> employees = new ArrayList<Employee>();
        assertThat(Expression.find(employees).where(EmployeeAge.is(Equal.to(1))).execute()).isEqualTo(employees);
    }

    @Test
    public void itCreatesACountExpression() {
        Employee foo = new Employee("foo", 1);
        Employee bar = new Employee("bar", 2);
        Employee baz = new Employee("baz", 2);
        List<Employee> employees = Arrays.asList(foo, bar, baz);
        assertThat(Expression.countOf(employees).where(EmployeeAge.is(Equal.to(2))).and(EmployeeName.is(Equal.to("baz"))).execute()).isEqualTo(1);

    }

}
