import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class EmployeeEqualAgeSpecs {
    @Test
    public void itSatisfiesEmployeeAge() {
        assertThat(EmployeeAge.is(Equal.to(5)).satisfy(new Employee(null, 5))).isTrue();
        assertThat(EmployeeAge.is(Equal.to(5)).satisfy(new Employee(null, 6))).isFalse();
    }
}
