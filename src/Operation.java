public abstract class Operation {
    protected final Object comparator;

    public Operation(Object comparator) {
        this.comparator = comparator;
    }

    public abstract boolean evaluate(Object object);
}
