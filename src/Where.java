import java.util.List;

public class Where {
    protected final List<Employee> employees;
    protected final EmployeeCriteria employeeCriteria;
    protected final Where where;

    public Where(List<Employee> employees, EmployeeCriteria employeeCriteria, Where where) {
        this.employees = employees;
        this.employeeCriteria = employeeCriteria;
        this.where = where;
    }

    protected boolean satisfy(Employee employee) {
        return where.satisfy(employee) &&  employeeCriteria.satisfy(employee);
    }

    protected void execute(Strategy strategy) {
        for (Employee employee : employees) {
            if(satisfy(employee))
                strategy.use(employee);
        }
    }
}
