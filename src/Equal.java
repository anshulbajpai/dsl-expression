public class Equal extends Operation {

    private Equal(Object comparator) {
        super(comparator);
    }

    public static Equal to(Object comparator) {
        return new Equal(comparator);
    }

    @Override
    public boolean evaluate(Object object) {
        return object.equals(comparator);
    }
}
