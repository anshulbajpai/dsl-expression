import java.util.ArrayList;
import java.util.List;

public class FindStrategy implements  Strategy{

    private final List<Employee> employees = new ArrayList<Employee>();

    @Override
    public void use(Employee employee) {
        employees.add(employee);
    }

    public List<Employee> getEmployees() {
        return employees;
    }
}