public class EmployeeAge extends EmployeeCriteria {

    private EmployeeAge(Operation operation) {
        super(operation);
    }

    public static EmployeeCriteria is(Operation equal) {
        return new EmployeeAge(equal);
    }

    protected Object getValue(Employee employee) {
        return employee.getAge();
    }
}
