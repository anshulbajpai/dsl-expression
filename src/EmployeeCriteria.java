public abstract class EmployeeCriteria {
    protected final Operation operation;

    public EmployeeCriteria(Operation operation) {
        this.operation = operation;
    }

    public boolean satisfy(Employee employee) {
        return operation.evaluate(getValue(employee));
    }

    protected abstract Object getValue(Employee employee);
}
