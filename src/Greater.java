import java.math.BigDecimal;

public class Greater extends Operation {

    private Greater(Object comparator) {
        super(comparator);
    }

    public static Greater than(Object comparator) {
        return new Greater(comparator);
    }

    public boolean evaluate(Object object) {
        return toBigDecimal(object).compareTo(toBigDecimal(comparator)) == 1;
    }

    private BigDecimal toBigDecimal(Object object) {
        return new BigDecimal(object.toString());
    }
}
