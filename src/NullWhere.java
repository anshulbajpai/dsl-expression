public class NullWhere extends Where {
    public NullWhere() {
        super(null, null, null);
    }

    @Override
    protected boolean satisfy(Employee employee) {
        return true;
    }
}
