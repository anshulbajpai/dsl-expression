import java.util.List;

public class Find extends Expression{

    public Find(List<Employee> employees) {
        super(employees);
    }

    public FindWhere where(EmployeeCriteria employeeCriteria) {
        return new FindWhere(employees,employeeCriteria);
    }
}
