public class CountStrategy implements Strategy {
    
    private int count = 0;
    
    @Override
    public void use(Employee employee) {
        count++;
    }
    
    public int getCount(){
        return count;
    }
}
