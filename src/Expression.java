import java.util.List;

public  class Expression {
    protected final List<Employee> employees;

    public Expression(List<Employee> employees) {
        this.employees = employees;
    }

    public static  Find find(List<Employee> employees) {
        return new Find(employees);
    }

    public static Count countOf(List<Employee> employees) {
        return new Count(employees);
    }
}
