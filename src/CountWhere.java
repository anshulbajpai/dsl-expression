import java.util.List;

public class CountWhere extends Where{

    public CountWhere(List<Employee> employees, EmployeeCriteria employeeCriteria) {
        this(employees, employeeCriteria, new NullWhere());
    }

    private CountWhere(List<Employee> employees, EmployeeCriteria employeeCriteria, Where where) {
        super(employees, employeeCriteria, where);
    }

    public int execute() {
        CountStrategy strategy = new CountStrategy();
        execute(strategy);
        return strategy.getCount();
    }

    public CountWhere and(EmployeeCriteria employeeCriteria) {
        return new CountWhere(employees, employeeCriteria, this);
    }

}
