import java.util.ArrayList;
import java.util.List;

public class FindWhere extends Where {


    public FindWhere(List<Employee> employees, EmployeeCriteria employeeCriteria) {
        this(employees, employeeCriteria, new NullWhere());
    }

    private FindWhere(List<Employee> employees, EmployeeCriteria employeeCriteria, Where where) {
        super(employees, employeeCriteria, where);
    }

    public List<Employee> execute() {
        FindStrategy strategy = new FindStrategy();
        execute(strategy);
        return strategy.getEmployees();
    }

    public FindWhere and(EmployeeCriteria employeeCriteria) {
        return new FindWhere(employees, employeeCriteria, this);
    }
}
