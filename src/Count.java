import java.util.List;

public class Count extends  Expression{

    public Count(List<Employee> employees) {
        super(employees);
    }

    public CountWhere where(EmployeeCriteria employeeCriteria) {
        return new CountWhere(employees, employeeCriteria);
    }
}
