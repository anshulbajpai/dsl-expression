public class EmployeeName extends EmployeeCriteria {

    private EmployeeName(Operation operation) {
        super(operation);
    }

    public static EmployeeCriteria is(Operation equal) {
        return new EmployeeName(equal);
    }

    protected String getValue(Employee employee) {
        return employee.getName();
    }
}
