public interface Strategy{
    void use(Employee employee);
}