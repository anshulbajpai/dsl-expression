import java.math.BigDecimal;

public class Lesser extends Operation {

    private Lesser(Object comparator) {
        super(comparator);
    }

    public static Lesser than(Object comparator) {
        return new Lesser(comparator);
    }

    public boolean evaluate(Object object) {
        return toBigDecimal(object).compareTo(toBigDecimal(comparator)) == -1;
    }

    private BigDecimal toBigDecimal(Object object) {
        return new BigDecimal(object.toString());
    }
}
